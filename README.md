# PrimeNG Prestige

1) Para correr el proyecto, en la maquina local se debe tener intalado Node Js. Angular CLI

Para instalar NODE JS, irse al siguiente link https://nodejs.org/es/download/ y darle siguiente.

Para instalar Angular
npm install -g @angular/cli

2) Fronted elaborado en Angular 13. 
3) Recomendación usar Intellij Idea.
4) Al abrir el proyecto ejecutar el siguiente comando desde la terminal
npm install
5) Esperar que instale todas las librerias y ejecutar el proyecto desde la terminal
ng s -o

6) Cuando abra el proyecto en el navegador, les abrira la pagina en blanco. Seleccionar el menu en la parte de arriba que dice VACUNAS o bien dirigirse a la URL http://localhost:4200/#/vacunas
-Se cumple con la funcionalidad solicitada.
