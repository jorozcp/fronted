import {Component, OnInit} from '@angular/core';
import {EmpleadoPuestoService} from '../../_service/empleado-puesto.service';
import {EmpleadoPuesto} from '../../_model/empleadoPuesto';
import {EmpleadoVacuunadoService} from '../../_service/empleadoVacuunado.service';
import {EmpleadosVacunados} from '../../_model/empleadosVacunados';
import {ConfirmationService, MessageService} from 'primeng/api';
import {VacunCovid19} from '../../_model/vacunCovid19';
import {DosisDTO} from '../../_model/dosisDTO';
import {VacunaEstado} from '../../_model/vacunaEstado';
import {switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-inicio',
    templateUrl: './inicio.component.html',
    styleUrls: ['./inicio.component.scss'],
    providers: [MessageService, ConfirmationService]
})

export class InicioComponent implements OnInit {

    display: boolean = false;
    empleadosPuesto: EmpleadoPuesto[];
    empleadosVacunados: EmpleadosVacunados[];
    empleadoEliminar: EmpleadosVacunados;
    empleadosFiltrados: EmpleadosVacunados[];
    empleadoSeleccionado: EmpleadosVacunados = new EmpleadosVacunados();
    nuevoEmpleadoVacunado: EmpleadosVacunados = new EmpleadosVacunados();
    vacunas: VacunCovid19[];
    dosisSeleccionada: number;
    dosis: DosisDTO[];
    estadoEmpleado: VacunaEstado = new VacunaEstado();
    empleadoModificar: EmpleadosVacunados = new EmpleadosVacunados();
    displayModificar: boolean = false;
    estadoModificar: number;
    janssen: boolean = false;

    constructor(
        private empleadosPuestoService: EmpleadoPuestoService,
        private empleadosVacunadosService: EmpleadoVacuunadoService,
        private confirmationService: ConfirmationService,
        private messageService: MessageService,
    ) {
        this.dosis = [
            {idDosis: '0', dosis: '0'},
            {idDosis: '1', dosis: '1'},
            {idDosis: '2', dosis: '2'},
        ];
    }

    ngOnInit(): void {
        this.empleadosVacunadosService.getEmpleadosVacunadosCambios().subscribe(data => {
            this.empleadosVacunados = data;
        });
        this.listar();
    }

    listar() {
        this.empleadosVacunadosService.listarEmpleadosVacunados().subscribe(data => {
            this.empleadosVacunados = data;
        });
        this.empleadosPuestoService.listarEmpleados().subscribe(data => {
            this.empleadosPuesto = data;
        });
        this.empleadosVacunadosService.listadoVacunas().subscribe(data => {
            this.vacunas = data;
        });
    }


    modalRegistroEmpleado() {
        this.dosisSeleccionada = null;
        this.estadoEmpleado = new VacunaEstado();
        this.empleadoSeleccionado = new EmpleadosVacunados();
        this.nuevoEmpleadoVacunado = new EmpleadosVacunados();
        this.display = true;
    }

    deleteProduct(empleadosVacunados1: EmpleadosVacunados) {
        this.confirmationService.confirm({
            message: 'Esta seguro que quiere eliminar al empleado ' + empleadosVacunados1.empleado.nombre + '?',
            header: 'Confirmar',
            icon: 'pi pi-exclamation-triangle',
            acceptLabel: 'Si',
            accept: () => {
                this.empleadoEliminar = this.empleadosVacunados.find(val => val.idEmpleado == empleadosVacunados1.idEmpleado);
                this.empleadosVacunadosService.eliminarEmpleado(this.empleadoEliminar.idEmpleado).pipe(switchMap(() => {
                    return this.empleadosVacunadosService.listarEmpleadosVacunados();
                })).subscribe(data => {
                    this.empleadosVacunadosService.setEmpleadosVacunadosCambios(data);
                    this.messageService.add({severity: 'success', summary: 'Exito', detail: 'Empleado Eliminado', life: 3000});
                });
            }
        });
    }

    filterCountry(event) {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let listaTemporal: any[] = [];
        let query = event.query;

        for (let i = 0; i < this.empleadosPuesto.length; i++) {
            let empleado = this.empleadosPuesto[i];
            if (empleado.empleado.nombre.toLowerCase().indexOf(query.toLowerCase()) == 0 || empleado.empleado.apellido.toLowerCase().indexOf(query.toLowerCase()) == 0 || empleado.puesto.nombrePuesto.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                listaTemporal.push(empleado);
            }
        }

        this.empleadosFiltrados = listaTemporal;
    }

    camposRequeridos() {
        if (this.dosisSeleccionada == 1) {
            return (this.nuevoEmpleadoVacunado.idVacuna == null || this.nuevoEmpleadoVacunado.fechaInicioPuesta == null);
        }
        if (this.dosisSeleccionada == 2) {
            return (this.nuevoEmpleadoVacunado.idVacuna == null || this.nuevoEmpleadoVacunado.fechaInicioPuesta == null || this.nuevoEmpleadoVacunado.fechaFinPuesta == null);
        }
        return this.dosisSeleccionada == null;
    }

    guardar() {
        this.empleadosVacunadosService.verificarEmpleadoAgregado(this.empleadoSeleccionado.idEmpleado).subscribe(data => {
            if (data == null) {
                this.nuevoEmpleadoVacunado.idEmpleado = this.empleadoSeleccionado.idEmpleado;
                if (this.nuevoEmpleadoVacunado.idEmpleado != undefined) {
                    this.nuevoEmpleadoVacunado.idEstado = this.estadoEmpleado.idEstado;
                    this.empleadosVacunadosService.registarEmpleadoVacunado(this.nuevoEmpleadoVacunado).pipe(switchMap(() => {
                        return this.empleadosVacunadosService.listarEmpleadosVacunados();
                    })).subscribe(data => {
                        this.empleadosVacunadosService.setEmpleadosVacunadosCambios(data);
                        this.messageService.add({severity: 'success', summary: 'Exito', detail: 'Empleado agregado a lista', life: 3000});
                        this.display = false;
                    });
                } else {
                    this.messageService.add({
                        severity: 'error', summary: 'Error', detail: 'Favor seleccionar al empleado', life: 3000, key: 'empleadoNoAgregado'
                    });
                }
            }else{
                this.messageService.add({severity: 'error', summary: 'Error', detail: 'Empleado ya existe en la lista', life: 3000, key:'EmpleadoYaExiste'});
            }
        });
    }

    cambioDosis(idEstado: number) {
        this.empleadosVacunadosService.listadoEstados(idEstado).subscribe(data => {
            this.estadoEmpleado = data;
        });
        if (idEstado == 0) {
            this.nuevoEmpleadoVacunado.idVacuna = null;
        }
        this.nuevoEmpleadoVacunado.fechaInicioPuesta = null;
        this.nuevoEmpleadoVacunado.fechaFinPuesta = null;
    }

    cambioDosisModfiicar(idEstado: number) {
        this.empleadosVacunadosService.listadoEstados(idEstado).subscribe(data => {
            this.estadoEmpleado = data;
        });
        if (idEstado == 0) {
            this.empleadoModificar.idVacuna = null;
        }
        this.empleadoModificar.fechaInicioPuesta = null;
        this.empleadoModificar.fechaFinPuesta = null;
    }

    cancelarGuardar() {
        this.display = false;
    }

    abrirModalModificar(empleado: EmpleadosVacunados) {
        this.estadoModificar = empleado.idVacuna;
        this.dosisSeleccionada = null;
        this.empleadoModificar = {...empleado};
        this.displayModificar = true;
    }

    modificar() {
        if (this.estadoModificar == null) {
            this.empleadoModificar.idEstado = this.estadoEmpleado.idEstado;
        }
        this.empleadosVacunadosService.modificarEmpleadoVacunado(this.empleadoModificar).pipe(switchMap(() => {
            return this.empleadosVacunadosService.listarEmpleadosVacunados();
        })).subscribe(data => {
            this.empleadosVacunadosService.setEmpleadosVacunadosCambios(data);
            this.messageService.add({severity: 'success', summary: 'Exito', detail: 'Empleado modificado exitosamente', life: 3000});
            this.displayModificar = false;
        });
    }

    cancelarModificar() {
        this.displayModificar = false;
    }

    modificarDisabled() {
        return this.empleadoModificar.idEstado == 2;
    }

    vacunaCambio() {
        if (this.nuevoEmpleadoVacunado.idVacuna == 6) {
            this.janssen = true;
            this.dosisSeleccionada = 1;
            this.estadoEmpleado.estado = 'PROTEGIDO';
        } else {
            this.janssen = false;
            this.dosisSeleccionada = null;
        }
    }

    vacunaCambioModificacion() {
        if (this.empleadoModificar.idVacuna == 6) {
            this.janssen = true;
            this.dosisSeleccionada = 1;
            this.estadoEmpleado.estado = 'PROTEGIDO';
            this.estadoEmpleado.idEstado = 2;
        } else {
            this.janssen = false;
            this.dosisSeleccionada = null;
        }
    }


}
