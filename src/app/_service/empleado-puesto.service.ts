import { Injectable } from '@angular/core';
import {GenericService} from './generic.service';
import {EmpleadoPuesto} from '../_model/empleadoPuesto';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoPuestoService extends GenericService<EmpleadoPuesto>{

  constructor(protected http: HttpClient) {
    super (http, `${environment.HOST}/empleadosPuesto`)
  }

  listarEmpleados(){
    return this.http.get<EmpleadoPuesto[]>(`${this.url}/listarEmpleados`)
  }
}
