import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GenericService} from './generic.service';
import {environment} from '../../environments/environment';
import {EmpleadosVacunados} from '../_model/empleadosVacunados';
import {VacunCovid19} from '../_model/vacunCovid19';
import {VacunaEstado} from '../_model/vacunaEstado';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoVacuunadoService extends GenericService<EmpleadosVacunados>{

  private empleadosVacunadosCambio = new Subject<EmpleadosVacunados[]>();

  constructor(protected http: HttpClient) {
    super (http, `${environment.HOST}/empleadosVacunados`)
  }

  registarEmpleadoVacunado(empleadoVacunado: EmpleadosVacunados){
    return this.http.post<EmpleadosVacunados>(`${this.url}/registarEmpleadoVacunado`, empleadoVacunado)
  }

  modificarEmpleadoVacunado(empleadoVacunado: EmpleadosVacunados){
    return this.http.put<EmpleadosVacunados>(`${this.url}/modificarEmpleadoVacunado`, empleadoVacunado)
  }

  listarEmpleadosVacunados(){
    return this.http.get<EmpleadosVacunados[]>(`${this.url}/listarEmpleadosVacunados`)
  }

  eliminarEmpleado(idEmpleado: number){
    return this.http.delete(`${this.url}/eliminarEmpleadoVacunado/${idEmpleado}`)
  }

  listadoVacunas(){
    return this.http.get<VacunCovid19[]>(`${this.url}/listadoVacunas`)
  }

  verificarEmpleadoAgregado(idEmpleado: number){
    return this.http.get<EmpleadosVacunados>(`${this.url}/verificarEmpleadoAgregado/${idEmpleado}`)
  }

  listadoEstados(idEstado: number){
    return this.http.get<VacunaEstado>(`${this.url}/listarEstado/${idEstado}`)
  }

  setEmpleadosVacunadosCambios(empleadosVacunados: EmpleadosVacunados[]){
    this.empleadosVacunadosCambio.next(empleadosVacunados);
  }

  getEmpleadosVacunadosCambios(){
    return this.empleadosVacunadosCambio.asObservable();
  }
}
