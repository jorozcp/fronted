export class DepartamentoPuesto{

    IdInventario: number;
    idPuesto: number;
    nombrePuesto: string;
    descripcion: string;
    sueldoMinimo: number;
    sueldoMaximo: number
}
