import {Empleado} from './empleado';
import {DepartamentoPuesto} from './departamentoPuesto';

export class EmpleadoPuesto{

    idEmpleado: number;
    idDepartamento: number;
    idPuesto: number;
    sueldo: number;
    fechaInicio: string;
    fechaFin: string

    empleado: Empleado;
    puesto: DepartamentoPuesto;

}
