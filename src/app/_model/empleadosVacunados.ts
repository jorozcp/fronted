import {Empleado} from './empleado';
import {EmpleadoPuesto} from './empleadoPuesto';
import {VacunCovid19} from './vacunCovid19';
import {VacunaEstado} from './vacunaEstado';

export class EmpleadosVacunados{

    idEmpleado: number;
    idVacuna: number;
    fechaInicioPuesta: string;
    fechaCalculada: string;
    fechaFinPuesta: string;
    idEstado: number;
    dosis: number;

    empleadoPuesto: EmpleadoPuesto;
    empleado: Empleado
    vacunaCovid19: VacunCovid19;
    vacunasEstado: VacunaEstado;
}
