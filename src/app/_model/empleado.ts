export class Empleado{

    idEmpleado: number;
    nombre: string;
    apellido: string;
    dpi: number;
    nit: string;
    estado: boolean;
    fechaNacimiento: string;
    telefono1: number;
    telefono2: number
}
