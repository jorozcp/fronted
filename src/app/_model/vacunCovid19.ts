export class VacunCovid19{

    idVacuna: number;
    nombreVacuna: string;
    dosis: number;
    intervalo: string;
    intervaloTiempo: number;
}
